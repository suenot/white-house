$(document).ready(function(){
	// nav-tabs
	$('#request a').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
	});
	// modal-select
	$('.modal-select a').click(function(e) {
		e.preventDefault();
		$(this).tab('show');
	});
	// slider in index
	$('.bxslider').bxSlider({
		mode: 'fade',
		onSliderLoad: function(){
			$('.bxslider-content, .bx-controls').show();
			$('.slider').addClass('slider-shadow');
		}
	});
	// slider in object-card
	$('#viewer').is(function(){
		$(this).timerGallery({
			easing: 'easeInOutCubic',
			timer : false
		});
	});
	// calc
	$('.ipotech-calc').hide()
	$('.ipotech-calc-button').click(function(){
		$('.ipotech-calc').toggle();
	});
	$('.credit-calc').hide()
	$('.credit-calc-button').click(function(){
		$('.credit-calc').toggle();
	});
	// advanced search 
	$('.advansed-search-box').hide()
	$('.advansed-search').click(function(){
		$('.advansed-search-box').toggle();
	});
	// object
	$('.reserved-objects-toggle').click(function(e){
		e.preventDefault();
		$('.reserved-objects').toggleClass('active')
	});
});